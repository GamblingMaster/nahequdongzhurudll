#pragma once
#include "Drive.h"
#include "Drive_Statement.h"

void *ZwProtectVirtualMemory_Func = NULL;
void *LdrLoadDll_Func = NULL;
void *ZwTestAlert_Func = NULL;
KEVENT wait_event;
PEPROCESS Process;

NTSTATUS Register_Load_Image()
{
	NTSTATUS status = PsSetLoadImageNotifyRoutine(Load_Image);
	return status;
}

void Load_Image(
	_In_ PUNICODE_STRING FullImageName,
	_In_ HANDLE ProcessId,                // pid into which image is being mapped
	_In_ PIMAGE_INFO ImageInfo
	)
{
	if (ProcessId == (HANDLE)0 || ProcessId == (HANDLE)4) return;
	if (!FullImageName || !FullImageName->Length) return;
	if (ImageInfo->SystemModeImage)return;

	//DbgPrint("1:%ws", FullImageName->Buffer);
	if (!wcscmp(FullImageName->Buffer, L"\\SystemRoot\\System32\\ntdll.dll"))//\Windows\System32\usp10.dll \\SystemRoot\\System32\\ntdll.dll
	{
		//DbgPrint("2:%ws", FullImageName->Buffer);
		NTSTATUS status = PsLookupProcessByProcessId(ProcessId, &Process);
		if (NT_SUCCESS(status))
		{
			UCHAR *ProcessName = PsGetProcessImageFileName(Process);
			//DbgPrint("3:%s", (char*)Process + 0x16c);
			if (!strcmp((char*)ProcessName, "Dbgview.exe"))
			{
				DbgPrint("4:%s", (char*)ProcessName);
				KAPC_STATE apc;
				KeStackAttachProcess(Process, &apc);
				//void *ZwProtectVirtualMemory_Func = NULL;
				/*void *LdrLoadDll_Func = NULL;
				void *ZwTestAlert_Func = NULL;*/
				ZwProtectVirtualMemory_Func = BBGetModuleExport(ImageInfo->ImageBase, "ZwProtectVirtualMemory");
				LdrLoadDll_Func = BBGetModuleExport(ImageInfo->ImageBase, "LdrLoadDll");
				ZwTestAlert_Func = BBGetModuleExport(ImageInfo->ImageBase, "ZwTestAlert");
				DbgPrint("%X", ZwTestAlert_Func);
				KeUnstackDetachProcess(&apc);

				
				HANDLE thread_handle;
				PsCreateSystemThread(&thread_handle, THREAD_ALL_ACCESS, NULL, NULL, NULL,
					(PKSTART_ROUTINE)Write_Process_Memory, NULL);

				KeInitializeEvent(&wait_event,SynchronizationEvent,//SynchronizationEvent为同步事件  
					FALSE);
				KeWaitForSingleObject(&wait_event, Executive, KernelMode, FALSE, NULL);

			}
		}
	}

}

void Write_Process_Memory()
{
	char Shell_Code[] =
		"\x55" //push ebp 保存栈环境
		"\x8B\xEC" //mov ebp,esp
		//"\x50"//push eax
		"\x52"//push edx
		//第一个地址.偏移+5
		"\x68\x00\x00\x00\x00" //push 最后一个地址.handle的地址
		//第二个地址.偏移+9
		"\x68\x00\x00\x00\x00" //push 倒数第二个参数.此处为UNICODE的str地址
		"\x6A\x00" //push 0
		"\x6A\x00" //push 0
		//第三个地址.偏移+19
		"\xFF\x15\x00\x00\x00\x00" //FF 15 call 此处直接填变量地址.变量中储存函数地址.
		"\xB8\x74\x01\x00\x00" //mov eax,174h 存储SSDT表索引
		"\xBA\x00\x03\xFE\x7F" //mov edx,7FFE0300h 直接中断进内核层
		"\xFF\x12" //call dword ptr [edx]  call edx
		"\x33\xC0" //eax eax清零
		"\x5A"//pop edx
		//"\x58"//pop eax
		"\x5D" //pop ebp 还原堆栈
		"\xC3"; //C3 retrun
	void *Test = Shell_Code;

	CG_NtProtectVirtualMemory NtProtectVirtualMemory = *(CG_NtProtectVirtualMemory)*(PULONG)(TABLE_FUNCTION(0x00d7));
	CG_NtReadVirtualMemory NtReadVirtualMemory = *(CG_NtReadVirtualMemory)*(PULONG)(TABLE_FUNCTION(0x0115));
	CG_NtWriteVirtualMemory NtWriteVirtualMemory = *(CG_NtWriteVirtualMemory)*(PULONG)(TABLE_FUNCTION(0x018f));
	CG_NtAllocateVirtualMemory NtAllocateVirtualMemory = *(CG_NtAllocateVirtualMemory)*(PULONG)(TABLE_FUNCTION(0x0013));


	/*KPROCESSOR_MODE ke = ExGetPreviousMode();
	if (ke == UserMode)
	{
	PKTHREAD Kthread = (PKTHREAD)PsGetCurrentThread();
	char *ke_mode = (char*)((ULONG)Kthread + 0x13a);
	*ke_mode = KernelMode;
	ke = ExGetPreviousMode();
	}*/

	HANDLE ProcessHandle;
	NTSTATUS status=ObOpenObjectByPointer(Process, OBJ_KERNEL_HANDLE, NULL, PROCESS_ALL_ACCESS,
		NULL, KernelMode, &ProcessHandle);
	


	HANDLE U_Handle = 0;
	void* R3_U_Str = 0;
	ULONG R3_U_Str_Lenght = 4;
	status = ZwAllocateVirtualMemory(ProcessHandle, &R3_U_Str, NULL,
		&R3_U_Str_Lenght, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	status = NtWriteVirtualMemory(ProcessHandle, R3_U_Str, &U_Handle, 4, NULL);
	ULONG temp = (ULONG)R3_U_Str;
	RtlCopyMemory(Shell_Code + 5, &temp, 4);
	//*(ULONG*)(Shell_Code + 4) = R3_U_Str;

	wchar_t U_Str_t[] = L"TEST_DLL.dll";
	R3_U_Str = 0;
	R3_U_Str_Lenght = sizeof(U_Str_t);
	status = ZwAllocateVirtualMemory(ProcessHandle, &R3_U_Str, NULL,
		&R3_U_Str_Lenght, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	status = NtWriteVirtualMemory(ProcessHandle, R3_U_Str, U_Str_t, sizeof(U_Str_t), NULL);


	UNICODE_STRING U_Str;
	RtlInitUnicodeString(&U_Str, L"TEST_DLL.dll");
	U_Str.Buffer = (PWCHAR)R3_U_Str;
	R3_U_Str = 0;
	R3_U_Str_Lenght = sizeof(UNICODE_STRING);
	status = ZwAllocateVirtualMemory(ProcessHandle, &R3_U_Str, NULL,
		&R3_U_Str_Lenght, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	status = NtWriteVirtualMemory(ProcessHandle, R3_U_Str, &U_Str, sizeof(UNICODE_STRING), NULL);
	temp = (ULONG)R3_U_Str;
	RtlCopyMemory(Shell_Code + 10, &temp, 4);
	//*(ULONG*)(Shell_Code + 9) = R3_U_Str;

	ULONG U_Point = (ULONG)LdrLoadDll_Func;
	R3_U_Str = 0;
	R3_U_Str_Lenght = 4;
	status = ZwAllocateVirtualMemory(ProcessHandle, &R3_U_Str, NULL,
		&R3_U_Str_Lenght, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	status = NtWriteVirtualMemory(ProcessHandle, R3_U_Str, &U_Point, 4, NULL);
	temp = (ULONG)R3_U_Str;
	RtlCopyMemory(Shell_Code + 20, &temp, 4);
	//*(ULONG*)(Shell_Code + 19) = R3_U_Str;

	R3_U_Str = 0;
	R3_U_Str_Lenght = sizeof(Shell_Code);
	status = ZwAllocateVirtualMemory(ProcessHandle, &R3_U_Str, NULL,
		&R3_U_Str_Lenght, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	status = NtWriteVirtualMemory(ProcessHandle, R3_U_Str, Shell_Code, sizeof(Shell_Code), NULL);
	

	char Test_Code[5] = { 0 };
	Test_Code[0] = '\xE9';
	temp = (ULONG)R3_U_Str;
	temp = temp - (ULONG)ZwTestAlert_Func - 5;
	RtlCopyMemory(Test_Code + 1, &temp, 4);
	ULONG Protect_Size = 5;
	ULONG Old_Protect = 0;
	ULONG Temp_Protect = 0;
	void *Test_Test = ZwTestAlert_Func;
	status = NtProtectVirtualMemory(ProcessHandle, &Test_Test, &Protect_Size,
		PAGE_EXECUTE_READWRITE, &Old_Protect);
	status = NtWriteVirtualMemory(ProcessHandle, ZwTestAlert_Func, Test_Code, 5, NULL);
	status = NtProtectVirtualMemory(ProcessHandle, &Test_Test, &Protect_Size,
		Old_Protect, &Temp_Protect);
	
	
	

	ZwClose(ProcessHandle);
	ObDereferenceObject(Process);


	KeSetEvent(&wait_event, 0, TRUE);
}



NTSTATUS Unload__Load_Image()
{
	NTSTATUS status = PsRemoveLoadImageNotifyRoutine(Load_Image);
	return status;
}