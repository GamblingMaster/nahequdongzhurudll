#pragma once
#include <Ntifs.h>

#define SYSCALL_INDEX(ServiceFunction) (*(PULONG)((PUCHAR)ServiceFunction + 1))
//#define SYSCALL_FUNCTION(ServiceFunction)KeServiceDescriptorTable->ntoskrnl.ServiceTableBase[SYSCALL_INDEX(ServiceFunction)]
#define SYSCALL_FUNCTION(ServiceFunction) (ULONG)KeServiceDescriptorTable->ntoskrnl.ServiceTableBase + SYSCALL_INDEX(ServiceFunction) * 4
#define TABLE_FUNCTION(Service_Function_Index) (ULONG)KeServiceDescriptorTable->ntoskrnl.ServiceTableBase + Service_Function_Index * 4


typedef struct _LDR_DATA_TABLE_ENTRY
{
	LIST_ENTRY    InLoadOrderLinks;
	LIST_ENTRY    InMemoryOrderLinks;
	LIST_ENTRY   InInitializationOrderLinks;
	PVOID            DllBase;
	PVOID            EntryPoint;
	ULONG            SizeOfImage;
	UNICODE_STRING    FullDllName;
	UNICODE_STRING     BaseDllName;
	ULONG            Flags;
	USHORT            LoadCount;
	USHORT            TlsIndex;
	PVOID            SectionPointer;
	ULONG            CheckSum;
	PVOID            LoadedImports;
	PVOID            EntryPointActivationContext;
	PVOID            PatchInformation;
	LIST_ENTRY    ForwarderLinks;
	LIST_ENTRY    ServiceTagLinks;
	LIST_ENTRY    StaticLinks;
	PVOID            ContextInformation;
	ULONG            OriginalBase;
	LARGE_INTEGER    LoadTime;
} LDR_DATA_TABLE_ENTRY, *PLDR_DATA_TABLE_ENTRY;

typedef struct _KSYSTEM_SERVICE_TABLE
{
	PULONG  ServiceTableBase;        // SSDT (System Service Dispatch Table)的基地址
	PULONG  ServiceCounterTableBase; // 包含 SSDT 中每个服务被调用的次数
	ULONG   NumberOfService;     // 服务函数的个数, NumberOfService * 4 就是整个地址表的大小
	ULONG   ParamTableBase;          // SSPT(System Service Parameter Table)的基地址
} KSYSTEM_SERVICE_TABLE, *PKSYSTEM_SERVICE_TABLE;

typedef struct _KSERVICE_TABLE_DESCRIPTOR
{
	KSYSTEM_SERVICE_TABLE   ntoskrnl; // ntoskrnl.exe 的服务函数
	KSYSTEM_SERVICE_TABLE   win32k;   // win32k.sys 的服务函数(GDI32.dll/User32.dll 的内核支持)
	KSYSTEM_SERVICE_TABLE   notUsed1;
	KSYSTEM_SERVICE_TABLE   notUsed2;

} KSERVICE_TABLE_DESCRIPTOR, *PKSERVICE_TABLE_DESCRIPTOR;

extern "C" PKSERVICE_TABLE_DESCRIPTOR  KeServiceDescriptorTable;


//extern "C"
//NTSTATUS
//NTAPI
//ZwQueryVirtualMemory(
//IN HANDLE ProcessHandle,
//IN PVOID BaseAddress,
//IN MEMORY_INFORMATION_CLASS MemoryInformationClass,
//OUT PVOID MemoryInformation,
//IN ULONG MemoryInformationLength,
//OUT PULONG ReturnLength OPTIONAL);
//
//typedef enum _MEMORY_INFORMATION_CLASS {
//	MemoryBasicInformation,
//	MemoryWorkingSetList,
//	MemorySectionName,
//	MemoryBasicVlmInformation
//} MEMORY_INFORMATION_CLASS;
//
//typedef struct _MEMORY_BASIC_INFORMATION {
//	PVOID  BaseAddress;
//	PVOID  AllocationBase;
//	unsigned long  AllocationProtect;
//	SIZE_T RegionSize;
//	unsigned long  State;
//	unsigned long  Protect;
//	unsigned long  Type;
//} MEMORY_BASIC_INFORMATION, *PMEMORY_BASIC_INFORMATION;


typedef NTSTATUS (*CG_NtReadVirtualMemory)(
	IN HANDLE               ProcessHandle,
	IN PVOID                BaseAddress,
	OUT PVOID               Buffer,
	IN ULONG                NumberOfBytesToRead,
	OUT PULONG              NumberOfBytesReaded OPTIONAL
	);

typedef NTSTATUS(*CG_NtProtectVirtualMemory)(
	IN HANDLE ProcessHandle,
	IN OUT PVOID *BaseAddress,
	IN OUT PULONG ProtectSize,
	IN ULONG NewProtect,
	OUT PULONG OldProtect);

typedef NTSTATUS(*CG_NtWriteVirtualMemory)(
	IN HANDLE               ProcessHandle,
	IN PVOID                BaseAddress,
	IN PVOID                Buffer,
	IN ULONG                NumberOfBytesToWrite,
	OUT PULONG              NumberOfBytesWritten OPTIONAL);

typedef NTSTATUS(*CG_NtAllocateVirtualMemory)(
	IN HANDLE               ProcessHandle,
	IN OUT PVOID            *BaseAddress,
	IN ULONG                ZeroBits,
	IN OUT PULONG           RegionSize,
	IN ULONG                AllocationType,
	IN ULONG                Protect);

extern "C" NTSYSAPI UCHAR* NTAPI PsGetProcessImageFileName(
	__in PEPROCESS Process);



